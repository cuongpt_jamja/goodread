from django import forms


class CreateBookForm(forms.Form):
    title = forms.CharField(label='Title', max_length=100)
    desc = forms.CharField(label='Description', max_length=100)
    author = forms.CharField(label='Author', max_length=100)
