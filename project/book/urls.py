from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.list_book, name='list_book'),
    url(r'^book/create/$', views.create_book, name='create_book'),
    url(r'^book/(?P<uid>\w+)/$', views.detail_book, name='detail_book'),
]
