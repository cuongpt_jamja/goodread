from mongoengine.document import Document, DynamicEmbeddedDocument, \
    DynamicDocument, EmbeddedDocument
from mongoengine.fields import DateTimeField, ReferenceField, StringField, \
    EmbeddedDocumentField, IntField, ListField
from datetime import datetime


class Review(Document):
    content = StringField()
    book = ReferenceField('Book')
    writer = ReferenceField('User')

    def __unicode__(self):
        return self.content


class Book(Document):
    title = StringField()
    desc = StringField()
    author = StringField()
    uploader = ReferenceField('User')

    def __unicode__(self):
        return self.title
