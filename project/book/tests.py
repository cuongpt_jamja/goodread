from django.test import TestCase
from mongoengine.django.auth import User
import test_addons
from models import Book


class BookTest(test_addons.MongoTestCase):
    def test_display_list_book(self):
        # create user
        self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'cuongpt',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        })
        # new user create book
        self.client.post('/books/book/create/', {
            'title': 'aaa',
            'desc': 'xxx',
            'author': 'yyy',
        })
        self.client.post('/books/book/create/', {
            'title': 'bbb',
            'desc': 'kkk',
            'author': 'zzz',
        })
        # test if display successfully all created books
        response = self.client.get('/books/')
        self.assertContains(response, 'aaa')
        self.assertContains(response, 'xxx')
        self.assertContains(response, 'yyy')
        self.assertContains(response, 'bbb')
        self.assertContains(response, 'kkk')
        self.assertContains(response, 'zzz')
        self.assertContains(response, 'cuongpt')

    def test_display_detail_book(self):
        # create user
        self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'cuongpt',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        })
        # new user create book
        self.client.post('/books/book/create/', {
            'title': 'aaa',
            'desc': 'xxx',
            'author': 'yyy',
        })
        book = Book.objects.get(title='aaa')
        # logout user after create book
        self.client.get('/users/user/logout/')
        response = self.client.get('/books/book/{}/'.format(str(book.id)))
        # test display correctly the book, without authenticate
        self.assertContains(response, 'aaa')
        self.assertContains(response, 'xxx')
        self.assertContains(response, 'yyy')
        self.assertContains(response, 'cuongpt')

    def test_display_create_book(self):
        response = self.client.get('/books/book/create/')
        self.assertContains(response, 'Title')
        self.assertContains(response, 'Description')
        self.assertContains(response, 'Author')

    def test_create_book_fail_without_authen(self):
        response = self.client.post('/books/book/create/', {
            'title': 'aaa',
            'desc': 'xxx',
            'author': 'yyy',
        }, follow=True)
        self.assertContains(response, 'You need to login to create book')

    def test_create_book_successful_with_authen(self):
        # create user
        self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'xxx',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        })
        # new user create book
        response = self.client.post('/books/book/create/', {
            'title': 'aaa',
            'desc': 'xxx',
            'author': 'yyy',
        }, follow=True)
        # test if redirecting correctly
        self.assertContains(response, 'Book Info')
        # test if create book successful in database
        book = Book.objects.get(title='aaa')
        self.assertIsNotNone(book)


class ReviewTest(test_addons.MongoTestCase):
    def test_write_review(self):
        pass
