from django.shortcuts import render
from django.views.generic import ListView, DetailView
from models import Book
from .forms import CreateBookForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate


def list_book(request):
    books = Book.objects.all()
    return render(request, 'book/list_book.html', {'books': books})


def create_book(request):
    if request.method == 'POST':
        if request.user.is_authenticated():
            form = CreateBookForm(request.POST)
            if form.is_valid():
                book = Book.objects.create(
                    title=request.POST['title'],
                    desc=request.POST['desc'],
                    author=request.POST['author'],
                )
                book.uploader = request.user
                book.save()
                return HttpResponseRedirect(reverse('book:detail_book',
                                            kwargs={'uid': str(book.id)}))
        else:
            form = CreateBookForm()
            return render(request, 'book/create_book_form.html', {
                'form': form,
                'message': 'You need to login to create book',
            })
    else:
        form = CreateBookForm()
        return render(request, 'book/create_book_form.html', {'form': form})


def detail_book(request, uid):
    book = Book.objects.get(id=uid)
    return render(request, 'book/detail_book.html', {'book': book})

