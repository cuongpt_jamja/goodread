from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^user/$', views.create_user, name='create_user'),
    url(r'^user/profile/(?P<uid>\w+)/$', views.profile_user, name='profile_user'),
    url(r'^user/logout/$', views.logout_user, name='logout_user'),
    url(r'^user/login/$', views.login_user, name='login_user'),
]
