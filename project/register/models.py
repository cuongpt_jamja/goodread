from mongoengine.document import Document, DynamicEmbeddedDocument, \
    DynamicDocument, EmbeddedDocument
from mongoengine.fields import DateTimeField, ReferenceField, StringField, \
    EmbeddedDocumentField, IntField, ListField
from datetime import datetime
from django.contrib.auth.models import User


class ExtraUser(Document):
    age = IntField()
    gender = StringField()

    def __unicode__(self):
        return self.age
