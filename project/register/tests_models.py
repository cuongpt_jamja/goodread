from django.test import TestCase
from mongoengine.django.auth import User
import test_addons


class UserTests(test_addons.MongoTestCase):
    def test_display_register_form(self):
        response = self.client.get('/users/user/')
        self.assertContains(response, 'First Name')
        self.assertContains(response, 'Last Name')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Email')
        self.assertContains(response, 'Username')

    def test_create_user(self):
        response = self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'xxx',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        })
        user = User.objects.get(username='xxx')
        self.assertIsNotNone(user)

    def test_display_profile_page(self):
        response = self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'xxx',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        },  follow=True)
        self.assertContains(response, 'Welcome')
        self.assertContains(response, 'xxx')

    def test_logout_successful(self):
        self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'xxx',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        }, follow=True)
        response = self.client.get('/users/user/logout/', follow=True)
        self.assertContains(response, 'First Name')
        self.assertContains(response, 'Last Name')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Email')
        self.assertContains(response, 'Username')

    def test_display_login(self):
        response = self.client.get('/users/user/login/')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')

    def test_login_successful(self):
        self.client.post('/users/user/', {
            'first_name': 'cuong',
            'last_name': 'pham',
            'username': 'xxx',
            'email': 'cuong@gmail.com',
            'password': '123abc',
        })
        response = self.client.post('/users/user/login/', {
            'username': 'xxx',
            'password': '123abc',
        }, follow=True)
        self.assertContains(response, 'Welcome')
        self.assertContains(response, 'xxx')
        self.assertContains(response, 'pham')
        self.assertContains(response, 'cuong')
