from django.shortcuts import render
from models import ExtraUser
from .forms import RegisterForm, LoginForm
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from mongoengine.django.auth import User
from django.contrib.auth import login, authenticate, logout


def create_user(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            new_user = User.objects.create(
                first_name=request.POST['first_name'],
                last_name=request.POST['last_name'],
                username=request.POST['username'],
                email=request.POST['email'],
            )
            new_user.set_password(request.POST['password'])
            new_user.save()
            auth_user = authenticate(username=request.POST['username'],
                                     password=request.POST['password'])
            login(request, auth_user)
            return HttpResponseRedirect(reverse('register:profile_user',
                                                kwargs={"uid": str(new_user['id'])}))
    else:
        form = RegisterForm()
    return render(request, 'register/register.html', {'form': form})


def profile_user(request, uid):
    if request.user.is_authenticated():
        user = User.objects.get(id=uid)
        return render(request, 'register/profile.html', {'user': user})
    else:
        return HttpResponseRedirect(reverse('register:create_user'))


def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST['username'],
                                password=request.POST['password'])
            login(request, user)
            return HttpResponseRedirect(reverse('register:profile_user',
                                                kwargs={"uid": str(user['id'])}))
    else:
        form = LoginForm()
    return render(request, 'register/login.html', {'form': form})


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('register:create_user'))